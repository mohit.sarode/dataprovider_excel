package com.dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Passing_MultipleData_intoScript_UsingJxl {
	
	public static void main(String[] args) throws BiffException, IOException {
		
WebDriver driver = new ChromeDriver();
        

File f =new File("/home/mohit.sarode/Documents/TestData/TestData1.xls");

FileInputStream fis = new FileInputStream(f);
Workbook book = Workbook.getWorkbook(fis);
Sheet sh = book.getSheet("Sheet1");
//String celldata = sh.getCell(0, 0).getContents();
//System.out.println("Value Present:- "+celldata);


int rows = sh.getRows();
int columns= sh.getColumns();

 for(int i=1; i<rows; i++) {
	  String Username = sh.getCell(0, i).getContents();
	  String Password = sh.getCell(1, i).getContents();
	  
	  driver.get("https://demowebshop.tricentis.com/login");
		
		driver.findElement(By.id("Email")).sendKeys(Username);
      
      driver.findElement(By.id("Password")).sendKeys(Password);
      
      driver.findElement(By.xpath("//input[@value='Log in']")).click();
      
      driver.findElement(By.linkText("Log out")).click();
      
	}
	  
 }

       

}
